import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore';
import ExpensesPage from "./src/components/ExpensesPage";
import * as Font from 'expo-font';

const store = configureStore();

export default function App() {
  return (
      <Provider store={store}>
        <ExpensesPage />
      </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
