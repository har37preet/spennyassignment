export function setAssetLoaded(bool) {
    return {
        type: 'ASSETS_LOADED',
        assetsLoaded: bool
    };
}

export function getExpenses() {
    console.log("action dispatched")
    return {
        type: 'GET_EXPENSES'
    }
};
