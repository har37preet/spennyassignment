export function assetsHasLoaded(state = false, action) {
    switch (action.type) {
        case 'ASSETS_LOADED':
            return action.assetsLoaded;

        default:
            return state;
    }
}

export function HasExpenses(state = { }, action)  {
    switch (action.type) {
        case 'EXPENSES_RECEIVED':
            console.log(action.json)
            return { ...state, HasExpenses: action.json, isLoading: false }
        default:
            return state;
    }
};

export function isLoading(state = true, action) {
    switch (action.type) {
        case 'API_LOADED':
            return false;

        default:
            return state;
    }
}

