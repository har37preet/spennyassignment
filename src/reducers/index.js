import { combineReducers } from 'redux';

import {assetsHasLoaded, HasExpenses, isLoading} from "./expenses";

export default combineReducers({
    assetsHasLoaded, HasExpenses, isLoading
});