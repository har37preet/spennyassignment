import React, { Component } from 'react';
import {StyleSheet, SafeAreaView, View, Text, StatusBar, ActivityIndicator, FlatList, Dimensions, ScrollView} from "react-native";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Constants from 'expo-constants';
import * as Font from 'expo-font';
import {getExpenses, setAssetLoaded} from '../../actions/expenses';
import {normalizeHeight, normalizeWidth} from "../../utils/Utils";
import { AntDesign } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import {DropDownSelector} from "../DropDownSelector";
import { LineChart } from "react-native-chart-kit";
import {SpendingItem} from "../SpendingItem";
export const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get(
    'window',
);

class ExpensesPage extends Component {
    async componentDidMount() {
        this.props.getExpenses()

        await Font.loadAsync({
            'heavy': require('./../../../assets/fonts/Avenir-Heavy.ttf'),
            'black': require('./../../../assets/fonts/Avenir-Black.ttf'),
            'medium': require('./../../../assets/fonts/Avenir-Medium.ttf'),
            'roman': require('./../../../assets/fonts/Avenir-Roman.ttf'),
        });
        this.props.setAssetLoaded(true)
    }
    render() {
        if(this.props.assetsLoaded) {
            if(this.props.isLoading){
                return(
                    <ActivityIndicator/>
                )
            }else {
                return (
                    <ScrollView style={styles.container}>
                        <View style={styles.headerContainer}>
                            <Text style={styles.title}>Expenses</Text>
                            <View style={{flex: 1}}/>
                            <View style={{
                                backgroundColor: '#0047cc',
                                borderRadius: normalizeWidth(5),
                                marginRight: normalizeWidth(10),
                                position: 'absolute',
                                height: normalizeHeight(25),
                                width: normalizeHeight(25),
                                right: 0
                            }}/>
                            <View style={{marginRight: normalizeHeight(20)}}>
                                <Entypo name="arrow-bold-right" size={24} color="#0eafe6"/>
                            </View>
                        </View>
                        <View style={[styles.headerContainer]}>
                            <View style={{flex: 1}}>
                                <Text style={styles.cardBalancetitle}>Card Balance</Text>
                                <Text style={styles.cardBalance}>${this.props.expenses.HasExpenses.cardBalance}</Text>
                            </View>
                            <View>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <View style={{
                                        backgroundColor: '#e2f2eb',
                                        borderRadius: normalizeWidth(5),
                                        marginRight: normalizeWidth(10)
                                    }}>
                                        <AntDesign name="arrowup" size={18} color="#1bc773"/>
                                    </View>
                                    <Text style={[styles.CDBalanceTitle, {color: '#1bc773'}]}>$1234</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginTop: normalizeHeight(10),
                                    alignItems: 'center'
                                }}>
                                    <View style={{
                                        backgroundColor: '#f6e6e7',
                                        borderRadius: normalizeWidth(5),
                                        marginRight: normalizeWidth(10)
                                    }}>
                                        <AntDesign name="arrowdown" size={18} color="#f24750"/>
                                    </View>
                                    <Text style={[styles.CDBalanceTitle, {color: '#f24750'}]}>$1234</Text>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.headerContainer]}>
                            <View style={{marginRight: normalizeWidth(40)}}>
                                <DropDownSelector/>
                            </View>

                            <FlatList
                                horizontal
                                data={this.props.expenses.HasExpenses.month}
                                renderItem={({item,index}) =>
                                    <View style={{
                                        backgroundColor: index==0 ? '#dfe7f5' : '#f8f9f9',
                                        borderRadius: normalizeWidth(5),
                                        marginRight: normalizeWidth(10),
                                        padding: 5
                                    }}>
                                        <Text style={{
                                            color: index==0 ? '#0047cc' : '#77869e',
                                            fontFamily: 'medium',
                                            fontSize: normalizeHeight(18)
                                        }}>{item}</Text>
                                    </View>
                                }
                                key={(item, index) => index}
                            />
                        </View>

                        <LineChart
                            data={this.props.expenses.HasExpenses.data}
                            width={SCREEN_WIDTH}
                            height={normalizeHeight(220)}
                            yAxisInterval={1}
                            withHorizontalLabels={false}
                            withInnerLines={false}
                            chartConfig={{
                                backgroundGradientFrom: "#f8f9f9",
                                backgroundGradientTo: "#f8f9f9",
                                color: (opacity = 1) => '#77869e',
                                labelColor: (opacity = 1) => `#1d3041`,
                                decimalPlaces: 0,
                                style: {
                                    borderRadius: 16,
                                },
                                propsForDots: {
                                    r: "0",
                                    strokeWidth: "10",
                                    stroke: "#77869e"
                                }
                            }}
                            bezier
                            style={{
                                width: SCREEN_WIDTH,
                                borderRadius: 16,
                                backgroundColor: '#f8f9f9',
                                marginTop: normalizeHeight(20)
                            }}
                        />

                        <View style={[styles.headerContainer, {marginBottom: normalizeHeight(20)}]}>
                            <Text style={styles.title}>Spending Breakdown</Text>
                        </View>
                        <FlatList
                            data={this.props.expenses.HasExpenses.spending}
                            renderItem={({item}) =>
                                <View style={{alignItems: 'center', marginBottom: normalizeHeight(10)}}>
                                    <SpendingItem
                                        item={item}/>
                                </View>
                            }
                            key={(item) => item.date}
                        />
                    </ScrollView>
                )
            }
        }else{
            return (
                <View style={styles.container}>
                    <ActivityIndicator />
                    <StatusBar barStyle="default" />
                </View>
            );
        }
    }
}

ExpensesPage.propTypes = {
    setAssetLoaded: PropTypes.func.isRequired,
    getExpenses: PropTypes.func.isRequired,
    assetsLoaded: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    HasExpenses: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        assetsLoaded: state.assetsHasLoaded,
        expenses: state.HasExpenses,
        isLoading: state.isLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setAssetLoaded: (url) => dispatch(setAssetLoaded(url)),
        getExpenses: () => dispatch(getExpenses()),
    };
};

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#f8f9f9'
    },
    headerContainer: {marginTop: normalizeHeight(30), flexDirection: 'row', marginHorizontal: normalizeWidth(30), alignItems: 'center'},
    title: {
        color: '#042c5c',
        fontFamily: 'heavy',
        fontSize: normalizeHeight(24)
    },
    cardBalancetitle:{
        color: '#77869e',
        fontFamily: 'heavy',
        fontSize: normalizeHeight(18)
    },
    cardBalance:{
        color: '#042c5c',
        fontFamily: 'black',
        fontSize: normalizeHeight(32)
    },
    CDBalanceTitle:{
        fontFamily: 'black',
        fontSize: normalizeHeight(18),
    }
})

// export default ExpensesPage;
export default connect(mapStateToProps, mapDispatchToProps)(ExpensesPage);

