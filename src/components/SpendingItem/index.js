import React from 'react';
import {View, Text, Picker, TouchableNativeFeedback, Dimensions} from "react-native";
import {normalizeHeight, normalizeWidth} from "../../utils/Utils";
export const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get(
    'window',
);
import { Feather } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
export function SpendingItem(props){
    let type;

    let days = ["Sunday","Monday", "Tuesday", "Wednesday","Thursday","Friday","Saturday"];
    let months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    for (let k in props.item){
        if(k!=="date"){
            type = k;
        }
    }

    let renderImage = function () {
        switch (type) {
            case "Shopping":
                return(
                    <View style={{backgroundColor: '#ff7e87', borderRadius: normalizeWidth(5), marginRight: normalizeWidth(10), padding: normalizeHeight(20)}}>
                        <Feather name="shopping-cart" size={24} color="white" />
                    </View>
                )
                break;
            case "Bills":
                return(
                    <View style={{backgroundColor: '#ff7e87', borderRadius: normalizeWidth(5), marginRight: normalizeWidth(10), padding: normalizeHeight(20)}}>
                        <FontAwesome5 name="money-bill-wave-alt" size={24} color="white" />
                    </View>
                )
                break;
            case "Rent":
                return(
                    <View style={{backgroundColor: '#ff7e87', borderRadius: normalizeWidth(5), marginRight: normalizeWidth(10), padding: normalizeHeight(20)}}>
                        <Entypo name="home" size={24} color="white" />
                    </View>
                )
                break;
        }
    }
    return(
        <View style={{backgroundColor: '#ffffff', width: SCREEN_WIDTH*0.92, borderRadius: normalizeHeight(10), flexDirection: 'row', paddingHorizontal: normalizeWidth(15), paddingVertical: normalizeHeight(15), alignItems: 'center', justifyContent: 'center'}}>
            {renderImage()}
            <View style={{flex: 1, marginLeft: normalizeWidth(20)}}>
                <Text style={{fontFamily: 'heavy', color: '#042c5c', fontSize: normalizeHeight(20)}}>{type}</Text>
                <Text style={{fontFamily: 'roman', color: '#77869e', fontSize: normalizeHeight(18)}}>{(new Date(parseInt(props.item.date))).getDate()+ " " + days[(new Date(parseInt(props.item.date))).getDay()] + " " + months[(new Date(parseInt(props.item.date))).getUTCMonth()]}</Text>
            </View>
            <Text style={{fontFamily: 'heavy', color: '#ee5a55', fontSize: normalizeHeight(20)}}>- ${props.item[`${type}`]}</Text>

        </View>
    )
}