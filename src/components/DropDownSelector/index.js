import React from 'react';
import {View, Text, Picker, TouchableNativeFeedback} from "react-native";
import {normalizeHeight, normalizeWidth} from "../../utils/Utils";
import { MaterialIcons } from '@expo/vector-icons';

export function DropDownSelector(props){
    return(
        <TouchableNativeFeedback onPress={()=>{

        }}>
            <View style={{backgroundColor: '#ffffff', borderRadius: normalizeHeight(50), flexDirection: 'row', paddingHorizontal: normalizeWidth(20), paddingVertical: normalizeHeight(10), alignItems: 'center'}}>
                <Text style={{fontFamily: 'heavy', color: '#77869e', fontSize: normalizeHeight(18)}}>Monthly</Text>
                <View style={{marginLeft: normalizeWidth(10)}}>
                    <MaterialIcons name="keyboard-arrow-down" size={normalizeHeight(18)} color="#77869e" />
                </View>
            </View>
        </TouchableNativeFeedback>
    )
}