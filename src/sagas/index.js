import { put, takeLatest, all, call } from 'redux-saga/effects';

function* fetchExpenses() {
    const response = yield call(fetch, 'https://run.mocky.io/v3/2d7acdd7-cf46-4659-a6b3-c0a65f1ec439');
    const responseBody = yield response.json();
    yield put({ type: "EXPENSES_RECEIVED", json: responseBody, });
    yield put({ type: "API_LOADED" });
}
function* actionWatcher() {
    yield takeLatest('GET_EXPENSES', fetchExpenses)
}

export default function* root() {
    yield all([
        actionWatcher()
    ]);
}