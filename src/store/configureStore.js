import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas'

export default function configureStore(initialState) {

    const sagaMiddleware = createSagaMiddleware()

    const store = createStore( rootReducer, initialState, applyMiddleware(sagaMiddleware))

    sagaMiddleware.run(rootSaga)

    return store
}